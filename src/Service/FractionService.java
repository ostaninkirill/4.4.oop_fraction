package src.Service;

import src.models.Fraction;

public class FractionService implements IFractionService {


    @Override
    public Fraction simplification(Fraction f) {
        int num = f.getNumerator();
        int den = f.getDenominator();
        int gcm = gcm(num,den);
        return new Fraction((num / gcm), (den/gcm));
    }



    public int gcm(int a, int b) {
        return b == 0 ? a : gcm(b, a % b);
    }

    @Override
    public Fraction addition(Fraction f1, Fraction f2) {
        int newNum = f1.getNumerator() * f2.getDenominator() + f1.getDenominator() * f2.getNumerator();
        int newDen = f1.getDenominator() * f2.getDenominator();

        return simplification(new Fraction(newNum, newDen));
    }

    @Override
    public Fraction subtract(Fraction f1, Fraction f2) {
        int newNum = f1.getNumerator() * f2.getDenominator() - f1.getDenominator() * f2.getNumerator();
        int newDen = f1.getDenominator() * f2.getDenominator();

        return simplification(new Fraction(newNum, newDen));
    }

    @Override
    public Fraction multiplication(Fraction f1, Fraction f2) {
        int newNum = f1.getNumerator() * f2.getNumerator();
        int newDen = f1.getDenominator() * f2.getDenominator();
        return simplification(new Fraction(newNum, newDen));
    }

    @Override
    public Fraction division(Fraction f1, Fraction f2) {
        int newNum = f1.getNumerator() * f2.getDenominator();
        int newDen = f1.getDenominator() * f2.getNumerator();
        return simplification(new Fraction(newNum, newDen));
    }


}
