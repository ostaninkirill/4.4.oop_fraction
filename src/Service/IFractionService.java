package src.Service;

import src.models.Fraction;

public interface IFractionService {

    Fraction simplification (Fraction f);
    Fraction addition (Fraction f1, Fraction f2);
    Fraction subtract(Fraction f1, Fraction f2);
    Fraction multiplication(Fraction f1, Fraction f2);
    Fraction division(Fraction f1, Fraction f2);
}
