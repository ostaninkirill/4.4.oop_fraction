package src.Demo;

import src.Service.FractionService;
import src.Service.IFractionService;
import src.models.Fraction;

public class DemoService implements IDemoService {

    @Override
    public void execute() {
        IFractionService fractionService = new FractionService();
        Fraction fraction1 = new Fraction(31,248);
        Fraction fraction2 = new Fraction(588,126);

        System.out.println("Упрощение дроби " + fraction1.getNumerator() + " / " + fraction1.getDenominator() + " Результат: " + fractionService.simplification(fraction1));
        System.out.println("Упрощение дроби " + fraction2.getNumerator() + " / " + fraction2.getDenominator() + " Результат: " + fractionService.simplification(fraction2));


        System.out.println("Сложение (" + fraction1.getNumerator() + " / " + fraction1.getDenominator() + ") + (" + fraction2.getNumerator() + " / " + fraction2.getDenominator() +
                ") =  " + fractionService.addition(fraction1,fraction2));

        System.out.println("Вычитание (" + fraction2.getNumerator() + " / " + fraction2.getDenominator() + ") - (" + fraction1.getNumerator() + " / " + fraction1.getDenominator() +
        ") =  " + fractionService.subtract(fraction2,fraction1));

        System.out.println("Умножение (" + fraction2.getNumerator() + " / " + fraction2.getDenominator() + ") * (" + fraction1.getNumerator() + " / " + fraction1.getDenominator() +
                ") =  " + fractionService.multiplication(fraction2,fraction1));

        System.out.println("Деление (" + fraction2.getNumerator() + " / " + fraction2.getDenominator() + ") / (" + fraction1.getNumerator() + " / " + fraction1.getDenominator() +
                ") =  " + fractionService.division(fraction2,fraction1));

    }
}
